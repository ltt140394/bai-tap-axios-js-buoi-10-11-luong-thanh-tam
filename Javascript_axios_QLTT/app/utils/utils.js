// Ẩn hiện loading
const turnOnLoading = function () {
    document.getElementById("loading").style.display = "flex";
};

const turnOffLoading = function () {
    document.getElementById("loading").style.display = "none";
};

// Xóa dấu tiếng Việt nếu nhập trường dữ liệu là tên Việt để xử lý validate họ tên
function xoaDauTiengViet(str) {
    return str
        .normalize("NFD")
        .replace(/[\u0300-\u036f]/g, "")
        .replace(/đ/g, "d")
        .replace(/Đ/g, "D");
}

// Ẩn hiện button thêm hoặc cập nhật User phù hợp
document.getElementById("btnThemNguoiDung").addEventListener("click", function () {
    document.getElementById("btn-updateUser").style.display = "none";
    document.getElementById("btn-addUser").style.display = "block";
    document.getElementById("user-id").style.display = "none";
    document.getElementById("form-user").reset();
});

// Xóa các span thông báo Validate khi chưa submit và ấn button close trên form
document.getElementById("btn-close").addEventListener("click", function () {
    document.getElementById("spanTaiKhoan").innerText = "";
    document.getElementById("spanHoTen").innerText = "";
    document.getElementById("spanMatKhau").innerText = "";
    document.getElementById("spanEmail").innerText = "";
    document.getElementById("spanHinhAnh").innerText = "";
    document.getElementById("spanNguoiDung").innerText = "";
    document.getElementById("spanNgonNgu").innerText = "";
    document.getElementById("spanMoTa").innerText = "";
});

