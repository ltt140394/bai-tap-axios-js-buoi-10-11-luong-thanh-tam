const User = function (_account,_name,_password,_email,_imgURL,_type,_language,_description) {
    this.account = _account;
    this.name = _name;
    this.password = _password;
    this.email = _email;
    this.imgURL = _imgURL;
    this.type = _type;
    this.language = _language;
    this.description = _description;
}