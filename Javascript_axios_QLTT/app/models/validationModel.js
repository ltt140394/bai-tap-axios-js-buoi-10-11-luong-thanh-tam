var accountList = [];

const ValidatorUser = function () {
  this.checkEmptyText = function (idTarget, idErr, message) {
    var targetValue = document.getElementById(idTarget).value.trim();

    if (!targetValue) {
      document.getElementById(idErr).innerText = message;
      return false;
    } else {
      document.getElementById(idErr).innerText = "";
      return true;
    }
  };

  this.checkDuplicate = function () {

    userServices.getUserListData()
      .then(res => {
        (res.data).forEach(function (item) {
          accountList.push(item.account);
        });

      })
      .catch(err => {
        // console.log("err", err);
      });

    var valueTarget = document.getElementById("TaiKhoan").value.trim();

    var index = accountList.findIndex(function (item) {
      return item == valueTarget;
    });

    if (index == -1) {
      document.getElementById("spanTaiKhoan").innerText = "";
      return true;
    } else {
      document.getElementById("spanTaiKhoan").innerText = "Tài khoản đã được sử dụng";
      return false;
    }
  };

  this.checkName = function (idTarget, idErr) {
    var valueTarget = document.getElementById(idTarget).value.trim();
    var chuoiSauKhiXoaDau = xoaDauTiengViet(valueTarget);
    var regex = /^[a-zA-Z_ ]+$/;

    if (chuoiSauKhiXoaDau.match(regex)) {
      document.getElementById(idErr).innerText = "";
      return true;
    } else {
      document.getElementById(idErr).innerText =
        "Tên không được chứa số hoặc ký tự đặc biệt";
      return false;
    }
  };

  this.checkPassword = function (idTarget, idErr) {
    var valueTarget = document.getElementById(idTarget).value.trim();

    var regex = /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,8}$/;

    if (valueTarget.match(regex)) {
      document.getElementById(idErr).innerText = "";
      return true;
    } else {
      document.getElementById(idErr).innerText =
        "Mật khẩu không hợp lệ. Mật khẩu phải có ít nhất 1 ký tự hoa, 1 ký tự đặc biệt, độ dài 6-8 ký tự";
      return false;
    }
  };

  this.checkEmail = function (idTarget, idErr) {
    var valueTarget = document.getElementById(idTarget).value.trim();
    var regex =
      /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (valueTarget.match(regex)) {
      document.getElementById(idErr).innerText = "";
      return true;
    } else {
      document.getElementById(idErr).innerText = "Email không hợp lệ";
      return false;
    }
  };

  // Kiểm tra phải chọn 1 option nhất định trong select form
  this.checkSelectedOptions = function (idTarget, idErr, massage) {
    var valueTarget = document.getElementById(idTarget);

    if (valueTarget.selectedIndex == 0) {
      document.getElementById(idErr).innerText = massage;
      return false;
    } else {
      document.getElementById(idErr).innerText = "";
      return true;
    }
  };

  this.checkDescription = function (idTarget, idErr) {
    var valueTarget = document.getElementById(idTarget).value.trim();

    if (valueTarget.length > 60) {
      document.getElementById(idErr).innerText =
        "Mô tả không được quá 60 ký tự";
      return false;
    } else {
      document.getElementById(idErr).innerText = "";
      return true;
    }
  };
};

//Validate các trường thông tin trong form
const verifyForm = function () {
  var isValidAccount = validatorUser.checkEmptyText("TaiKhoan", "spanTaiKhoan", "Tài khoản không được để trống");
  var isValidName = validatorUser.checkEmptyText("HoTen", "spanHoTen", "Tên không được để trống") && validatorUser.checkName("HoTen", "spanHoTen");
  var isValidPassWord = validatorUser.checkEmptyText("MatKhau", "spanMatKhau", "Mật khẩu không được để trống") && validatorUser.checkPassword("MatKhau", "spanMatKhau");
  var isValidEmail = validatorUser.checkEmptyText("Email", "spanEmail", "Email không được để trống") && validatorUser.checkEmail("Email", "spanEmail");
  var isValidImg = validatorUser.checkEmptyText("HinhAnh", "spanHinhAnh", "Hình ảnh không được để trống");
  var isValidUserType = validatorUser.checkSelectedOptions("loaiNguoiDung", "spanNguoiDung", "Phải chọn 1 loại người dùng nhất định");
  var isValidLanguage = validatorUser.checkSelectedOptions("loaiNgonNgu", "spanNgonNgu", "Phải chọn 1 loại ngôn ngữ nhất định");
  var isValidDescription = validatorUser.checkEmptyText("MoTa", "spanMoTa", "Mô tả không được để trống") && validatorUser.checkDescription("MoTa", "spanMoTa");

  var isValidAll = isValidAccount && isValidName && isValidPassWord && isValidEmail && isValidImg && isValidUserType && isValidLanguage && isValidDescription;
  return isValidAll;
}




