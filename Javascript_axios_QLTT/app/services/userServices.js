const BASE_URL = "https://6271e181c455a64564b8ef66.mockapi.io";

const userServices = {
    getUserListData: function () {
        return axios({
            url: `${BASE_URL}/user`,
            method: "GET",
        });
    },

    addUser: function () {
        var newUser = getFormInputValue();
        return axios({
            url: `${BASE_URL}/user`,
            method: "POST",
            data: newUser,
        });
    },

    deleteUser: function (id) {
        return axios({
            url: `${BASE_URL}/user/${id}`,
            method: "DELETE",
        });
    },

    editUser: function (id) {
        return axios({
            url: `${BASE_URL}/user/${id}`,
            method: "GET",
        });
    },

    updateUser: function () {
        var userIsNeedUpdate = getFormInputValue();
        var userID = document.getElementById("user-id").innerText * 1;

        return axios({
            url: `${BASE_URL}/user/${userID}`,
            method: "PUT",
            data: userIsNeedUpdate,
        });
    },
}







