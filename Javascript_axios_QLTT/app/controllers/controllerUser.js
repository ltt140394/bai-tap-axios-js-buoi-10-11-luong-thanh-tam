// Render danh sách User ra giao diện
const renderUserList = function (userList) {
  var contentHTML = "";

  userList.forEach(function (item) {
    switch (item.language) {
      case "Sunday":
        item.language = "ITALIAN";
        break;
      case "Monday":
        item.language = "FRENCH";
        break;
      case "Tuesday":
        item.language = "JAPANESE";
        break;
      case "Wednesday":
        item.language = "CHINESE";
        break;
      case "Thursday":
        item.language = "RUSSIAN";
        break;
      case "Friday":
        item.language = "SWEDEN";
        break;
      case "Saturday":
        item.language = "SPANISH";
        break;
    }

    var contentTr = /*html */ `
    <tr>
    <td>${item.id}</td>
    <td>${item.account}</td>
    <td>${item.password}</td>
    <td>${item.name}</td>
    <td>${item.email}</td>
    <td>${item.imgURL}</td>
    <td>${item.language}</td>
    <td>${item.type ? "Giáo viên" : "Học viên"}</td>
    <td>${item.description}</td>
    <td>
    <button class="btn btn-danger" onclick="deleteUserServ(${item.id})">Xóa</button>
    <button class="btn btn-primary" onclick="editUserServ(${item.id})">Sửa</button>
    </td>
    </tr>
    `;
    contentHTML += contentTr;
  });

  document.getElementById("tblDanhSachNguoiDung").innerHTML = contentHTML;
};

//Lấy dữ liệu của User cần sửa
const getFormInputValue = function () {
  var account = document.getElementById("TaiKhoan").value;
  var name = document.getElementById("HoTen").value;
  var password = document.getElementById("MatKhau").value;
  var email = document.getElementById("Email").value;
  var imgURL = document.getElementById("HinhAnh").value;
  var type = document.getElementById("loaiNguoiDung").value;
  var description = document.getElementById("MoTa").value;
  var language = document.getElementById("loaiNgonNgu").value * 1;

  switch (language) {
    case 0:
      language = "Sunday";
      break;
    case 1:
      language = "Monday";
      break;
    case 2:
      language = "Tuesday";
      break;
    case 3:
      language = "Wednesday";
      break;
    case 4:
      language = "Thursday";
      break;
    case 5:
      language = "Friday";
      break;
    case 6:
      language = "Saturday";
      break;
  }

  return new User(account, name, password, email, imgURL, type == "GV" ? true : false, language, description
  );
};

//Xuất dữ liệu User cần sửa lên form để thao tác
const renderValueToForm = function (user) {
  document.getElementById("TaiKhoan").value = user.account;
  document.getElementById("HoTen").value = user.name;
  document.getElementById("MatKhau").value = user.password;
  document.getElementById("Email").value = user.email;
  document.getElementById("HinhAnh").value = user.imgURL;
  document.getElementById("loaiNguoiDung").value = user.type ? "GV" : "HV";
  document.getElementById("MoTa").value = user.description;

  var languageValue;
  switch (user.language) {
    case "Sunday":
      languageValue = 0;
      break;
    case "Monday":
      languageValue = 1;
      break;
    case "Tuesday":
      languageValue = 2;
      break;
    case "Wednesday":
      languageValue = 3;
      break;
    case "Thursday":
      languageValue = 4;
      break;
    case "Friday":
      languageValue = 5;
      break;
    case "Saturday":
      languageValue = 6;
      break;
  }

  document.getElementById("loaiNgonNgu").value = languageValue;
};






