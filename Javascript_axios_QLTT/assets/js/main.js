
var validatorUser = new ValidatorUser();

//Gọi API lấy data và render ra giao diện
const renderUserListServ = function () {
    turnOnLoading();

    userServices.getUserListData()
        .then((res) => {
            turnOffLoading();
            renderUserList(res.data);
        })
        .catch((err) => {
            turnOnLoading();
        });
};

renderUserListServ();


// Gọi API thêm user mới vào data
const addUserServ = function () {
 
    // em có viết hàm kiểm tra trùng Tài Khoản rồi (hàm checkDuplicate bên Validator) mà vì nó là bất đồng bộ xử lý phức tạp quá nên em không cho vào phần Validation ạ :(

    var isValidAll = verifyForm();
    if (isValidAll) {
        turnOnLoading();

        userServices.addUser()
            .then((res) => {
                $("#myModal").modal("hide");
                turnOffLoading();
                renderUserListServ();
            })
            .catch((err) => {
                turnOnLoading();
            });
    }
};

// Gọi API xóa user trong data
const deleteUserServ = function (id) {
    turnOnLoading();

    userServices.deleteUser(id)
        .then((res) => {
            turnOffLoading();
            renderUserListServ(res.data);
        })
        .catch((err) => {
            turnOnLoading();
        });
};

// Gọi API lấy thông tin User cần sửa
const editUserServ = function (id) {
    document.getElementById("btn-updateUser").style.display = "block";
    document.getElementById("btn-addUser").style.display = "none";
    document.getElementById("user-id").style.display = "none";

    turnOnLoading();

    userServices.editUser(id)
        .then((res) => {
            turnOffLoading();
            $("#myModal").modal("show");
            renderValueToForm(res.data);
            document.getElementById("user-id").innerText = res.data.id;
        })
        .catch((err) => {
            turnOnLoading();
        });
};

// Gọi API cập nhật thông tin User đã sửa lên data
const updateUserServ = function () {
    var isValidAll = verifyForm();

    if (isValidAll) {
        turnOnLoading();

        userServices.updateUser()
            .then((res) => {
                $("#myModal").modal("hide");
                turnOffLoading();
                renderUserListServ(res.data);
            })
            .catch((err) => {
                turnOnLoading();
            });
    }
};





